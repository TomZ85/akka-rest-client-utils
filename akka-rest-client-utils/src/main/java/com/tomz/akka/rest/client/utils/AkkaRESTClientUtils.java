package com.tomz.akka.rest.client.utils;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import akka.http.javadsl.Http;
import akka.http.javadsl.model.HttpEntity;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.util.ByteString;
import scala.concurrent.duration.FiniteDuration;

public class AkkaRESTClientUtils {
	public static <T> void callRESTService(final AkkaReferencies akkaReferencies, 
			final String urlWithParams, final Function<String, T> entityConverter) {
		final Materializer materializer = ActorMaterializer.create(akkaReferencies.getSystem());

		final CompletionStage<HttpResponse> responseFuture = Http.get(akkaReferencies.getSystem())
				.singleRequest(HttpRequest.create(urlWithParams), materializer);

		responseFuture.whenComplete((httpResponse, e) -> {
			final CompletionStage<HttpEntity.Strict> strictEntity = httpResponse.entity()
					.toStrict(FiniteDuration.create(3, TimeUnit.SECONDS).toMillis(), materializer);
			
			strictEntity.thenCompose(strict -> strict.getDataBytes()
					.runFold(ByteString.empty(), (acc, b) -> acc.concat(b), materializer)
					.thenApply(new Function<ByteString, T>() {

						@Override
						public T apply(ByteString t) {
							String entityString = new String(t.toArray());
							T response = entityConverter.apply(entityString);
							akkaReferencies.getSender().tell(response, akkaReferencies.getSelf());
							return response;
						}
					}));
		});
	}

}

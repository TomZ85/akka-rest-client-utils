package com.tomz.akka.rest.client.utils;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public class AkkaReferencies {
	private final ActorSystem system; 
	private final ActorRef sender;
	private final ActorRef self;

	public AkkaReferencies(ActorSystem system, ActorRef sender, ActorRef self) {
		super();
		this.system = system;
		this.sender = sender;
		this.self = self;
	}

	public ActorSystem getSystem() {
		return system;
	}

	public ActorRef getSender() {
		return sender;
	}

	public ActorRef getSelf() {
		return self;
	}
	
	
}
